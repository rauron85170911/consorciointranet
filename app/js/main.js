// JavaScript Document

$(document).ready(function () {


    $('#menu-pral ul.nav li.dropdown').hover(
        function () {
            var wndW = $(window).width();
            //console.log(wndW);
            if (wndW >= 992) {
                //console.log("mas");
                //		$(this).find('.dropdown-menu').stop(true, true).delay(300).fadeIn(100);
                //		$(this).toggleClass('drop_over');
            }
            if (wndW < 992) {
                //		console.log("menos");
            };

        },
        function () {
            var wndW = $(window).width();
            if (wndW >= 992) {
                //		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(100);
                //	  $(this).toggleClass('drop_over');
            }
            if (wndW < 992) {};
        }
    );


    $('#menu-pral ul.nav li.dropdown').click(
        function () {
            var wndW = $(window).width();
            //console.log(wndW);
            if (wndW >= 992) {
                //console.log("mas");
                //			$(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(100);
                //			$(this).toggleClass('drop_over');
            }
            if (wndW < 992) {
                //console.log("menos");
            };

        },
        function () {
            var wndW = $(window).width();
            if (wndW >= 992) {
                //		$(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(100);
                //	  $(this).toggleClass('drop_over');
            }
            if (wndW < 992) {};
        }
    );


    // ------------------------------ menú izquierda


    $('#menu-izq .bros .nav_nolink').click(function () {


        if ($(this).next().is(':hidden') == true) {
            console.log('menos');

            $(this).next().slideDown('slow', function () {
                $(this).parent().addClass('open');
            });
        } else {
            $(this).parent().removeClass('open');
            $(this).next().slideUp('slow', function () {
                $(this).parent().removeClass('open');
            });

            console.log('mas');

        }

    });




    /*$(function () {

        function desplegar(tl) {
            //console.log(this);
            tl.siblings('.dropdown').children('.dropdown-menu').stop(true, true).delay(0).slideUp(200); //recoge otros dropdowns sin delay
            tl.children('.dropdown-menu').stop(true, true).delay(100).slideDown(200);
            tl.toggleClass('drop_over');
        }

        function plegar(tl) {
            //console.log(tl);
            tl.children('.dropdown-menu').stop(true, true).delay(500).slideUp(200);
            tl.toggleClass('drop_over');
        }


        var ancho_min = 630;
        var ancho_med = 930;
        var ancho_max = 1280;

        $('li.dropdown').hover(function () {

                var wndW = $(window).width();
                var profundidad = $(this).parentsUntil('.nav', 'ul').length;
                var desplegado = $(this).parentsUntil('.open', 'ul').length;
                if (desplegado > profundidad) {
                    desplegado = profundidad
                }
                // 	 console.log("profundidad: " + profundidad);
                // 	 console.log("desplegado: " + desplegado);


                if ((wndW >= ancho_min) && (desplegado <= 1)) {
                    desplegar($(this));
                }

                if ((wndW >= ancho_med) && (desplegado <= 2)) {
                    desplegar($(this));
                }

                if ((wndW >= ancho_max) && (desplegado <= 3)) {
                    desplegar($(this));
                }

                if (wndW < ancho_min) {
                    // console.log('menos de 630');
                };
            }, function () {
                plegar($(this));

            }

        );

    }); */
    //-------------------------------- back to top


    $(window).scroll(function () {
        if ($(this).scrollTop() > 2000) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip();

    /* Datepicker */

    /* Inicialización en español para la extensión 'UI datepicker' para jQuery. */
    
    (function (factory) {
        if (typeof define === 'function' && define.amd) {

            // AMD. Register as an anonymous module.
            define(['../widgets/datepicker'], factory);
        } else {

            // Browser globals
            factory(jQuery.datepicker);
        }
    }(function(datepicker) {

        datepicker.regional.es = {
            closeText: 'Cerrar',
            prevText: '&#x3C;Ant',
            nextText: 'Sig&#x3E;',
            currentText: 'Hoy',
            monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio',
	'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
            monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun',
	'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
            dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
            dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
            dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        datepicker.setDefaults(datepicker.regional.es);

        return datepicker.regional.es;

    }));

    $(function () {
        $('#siniestro-desde').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#siniestro-desde').click(function(){
            $('#siniestro-desde').datepicker('show');
        });
        $('#siniestro-hasta').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#siniestro-hasta').click(function(){
            $('#siniestro-hasta').datepicker('show');
        });
        $('#alta-desde').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#alta-desde').click(function(){
            $('#alta-desde').datepicker('show');
        });
        $('#alta-hasta').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#alta-hasta').click(function(){
            $('#alta-hasta').datepicker('show');
        });
        $('#encargo-desde').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#encargo-desde').click(function(){
            $('#encargo-desde').datepicker('show');
        });
        $('#encargo-hasta').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#encargo-hasta').click(function(){
            $('#encargo-hasta').datepicker('show');
        });
        $('#fecha-siniestro').datepicker({
            showOn: 'button',
            buttonImage: './img/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Seleccione fecha',
        });
        $('#fecha-siniestro').click(function(){
            $('#fecha-siniestro').datepicker('show');
        });
    });
    
    /* Funcionlidad para los desplegables (Accordion) */
    
    var icons = {
      header: 'accord-arrow-right',
      activeHeader: 'accord-arrow-down'
    };
    $( '#desplegables' ).accordion({
        collapsible: true,
        autoHeight: false,
        heightStyle: 'content',
        navigation: true,
        icons:icons,
    });
    $( '#toggle' ).button().on( 'click', function() {
      if ( $( '#desplegables' ).accordion( 'option', 'icons' ) ) {
        $( '#desplegables' ).accordion( 'option', 'icons', null );
      } else {
        $( '#desplegables' ).accordion( 'option', 'icons', icons );
      }
    });
    $('#desplegables').accordion('option','active', 0);
    
    /* Funcionalidad sidebar */
    
    $('#buscador-desplegable').on('click', function () {
        var $contendorBuscador = $('#contenido-principal').find('aside');
        var $ContenidoSection = $('#contenido-principal').find('section');
        
        if (!$contendorBuscador.hasClass('close')) {
            // Cerramos
            $('#buscador-desplegable').removeClass('abierto');
            $('#buscador-desplegable').addClass('cerrado');
            $contendorBuscador.addClass('close');
            $ContenidoSection.addClass('expand');

        } else {
            // Abrimos
            $('#buscador-desplegable').removeClass('cerrado');
            $('#buscador-desplegable').addClass('abierto');
            $contendorBuscador.removeClass('close');
            $ContenidoSection.removeClass('expand');
        }
    });
    
    /* Funcionalidad para vaciar formularios con icono */
    
    $('#limpiar-estado-encargo').on('click',function(){
        $('#estado-encargo').val('');
    });
    
    $('#limpiar-tipo-dano').on('click',function(){
        $('#tipo-dano').val('');
    });
    
    $('#limpiar-uo-gabinete').on('click',function(){
        $('#uo-gabinete').val('');
    });
    
    $('#limpiar-rcm-gabinete').on('click',function(){
        $('#rcm-gabinete').val('');
    });
    
    $('#limpiar-tipo-tramitacion').on('click',function(){
        $('#tipo-tramitacion').val('0');
    });
    
    $('#limpiar-provincia').on('click',function(){
        $('#provincia').val('0');
    });
    
    $('#limpiar-localidad').on('click',function(){
        $('#localidad').val('');
    });
    
    $('#limpiar-codigo-postal').on('click',function(){
        $('#codigo-postal').val('');
    });

    $('#limpiar-tramitador').on('click',function(){
        $('#tramitador').val('');
    });
    
    $('#limpiar-identificacion').on('click',function(){
        $('#identificacion').val('');
    });
    
    $('#limpiar-expediente').on('click',function(){
        $('#expediente').val('');
    });
    
    $('#limpiar-referencia-perito').on('click',function(){
        $('#referencia-perito').val('');
    });
    
    $('#limpiar-referencia-aseguradora').on('click',function(){
        $('#referencia-aseguradora').val('');
    });
    
    $('#limpiar-referencia-aseguradora').on('click',function(){
        $('#referencia-aseguradora').val('');
    });
    
    $('#limpiar-vehiculo-danado').on('click',function(){
        $('#vehiculo-danado').val('');
    });
    
    /* Funcionalidad para cambiar el texto de los select por el valor de la label */
    
    var wndW = $(window).width();
    if(wndW < 480) {
        var $comboUO = $('#uo option:contains("---")');
        var $tipoActo = $('#tipo-acto option:contains("---")');
        var $tipoTramitacion = $('#tipo-tramitacion option:contains("---")');
        var $provincia = $('#provincia option:contains("---")');
        var $tipoEncargo = $('#tipo-encargo option:contains("---")');
        var $valPteEnviar = $('#valor-pte-enviar option:contains("---")');
        var $valorReiterada = $('#valor-reiterada option:contains("---")');
        var $via1 = $('#via1 option:contains("---")');
        var $causaSiniestro = $('#causa-siniestro option:contains("---")');
        
        $comboUO.text('U.O');
        $tipoActo.text('Tipo act.');
        $tipoTramitacion.text('Tipo tramitación');
        $provincia.text('Provincia');
        $tipoEncargo.text('Tipo encargo');
        $valPteEnviar.text('Val.pte.de Enviar');
        $valorReiterada.text('Val.reiterada');
        $via1.text('Via');
        $causaSiniestro.text('Causa Siniestro');
        
    } else {
    }
    
    /* Funcionalidad para posicionarse al inicio del body */
    
    function subirArriba() {
        $('html, body').animate({ scrollTop: 0 }, 600);
        return false;    
    }
    
    /* Funcionalidad para alertas */
    
    var $contenedorAlertas = $('#contenedor-alertas');
    var $alertaExito = '<div class="alerta-exito" role="alert">Mensaje informativo Positivo.Acción realizada.</div>';
    var $alertaNotificacion = '<div class="alerta-notificacion" role="alert">Mensaje informativo Neutro.Notificaciones sin caracter de exito en un proceso realizado.</div>';
    var $alertaErrorUsuario = '<div class="alerta-error-usuario" role="alert">Mensaje informativo negativo.Error del usuario, falta de información. etc...</div>';
    var $alertaErrorSistema = '<div class="alerta-error-sistema" role="alert">Mensaje de error de sistema.Error no atribuible al usuario.</div>';
    
    $('#accion-exito').on('click',function() {
        if($contenedorAlertas.find('div.alerta-exito').length) {
        }
        else {
            $contenedorAlertas.append($alertaExito);
            subirArriba();
            return true;
        }
    });
    $('#accion-notificacion').on('click',function() {
        if($contenedorAlertas.find('div.alerta-notificacion').length) {
        }
        else {
            $contenedorAlertas.append($alertaNotificacion);
            subirArriba();
            return true;
        }
    });
    $('#accion-error-usuario').on('click',function() {
        if($contenedorAlertas.find('div.alerta-error-usuario').length) {
        }
        else {
            $contenedorAlertas.append($alertaErrorUsuario);
            subirArriba();
            return true;
        }
    });
    $('#accion-error-sistema').on('click',function() {
        if($contenedorAlertas.find('div.alerta-error-sistema').length) {
        }
        else {
            $contenedorAlertas.append($alertaErrorSistema);
            subirArriba();
            return true;
        }
    });

}); /* Fin document.ready */